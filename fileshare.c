#include "main.h"
#include "fileshare.h"
#include<gtk/gtk.h>
#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netdb.h>
#include<pthread.h>
#include<netinet/in.h>
#include<unistd.h>
#include<stdbool.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/sendfile.h>
#include<sys/stat.h>
#include<arpa/inet.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

struct{
	cairo_surface_t *image;
}glob;

void loadfsProfileImage(cairo_t *cr);
gboolean on_fs_draw_event(GtkWidget *widget,cairo_t *cr,gpointer user_data){      
 	loadfsProfileImage(cr);
  	return FALSE;
}

void loadfsProfileImage(cairo_t *cr){
	GdkPixbuf *pixbuf;
	pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile.png", -1, 50, NULL);
	glob.image = gdk_cairo_surface_create_from_pixbuf(pixbuf,0,NULL);
  	cairo_set_source_surface(cr, glob.image, 1, 1);
  	cairo_arc(cr,24, 24, 23, 0, 2*M_PI);
  	cairo_clip(cr);
  	cairo_paint(cr); 
 	g_object_unref (pixbuf);
}

int checkExtensions(char *fileName){
	int len=strlen(fileName),i;
	for(i=(len-1);fileName[i]!='.' && i>=0;i--);
	char ext[30];
	strcpy(ext,fileName+i);
	if(strcmp(ext,".jpg")==0)
		return 2;
	else if(strcmp(ext,".png")==0)
		return 2;
	else if(strcmp(ext,".jpeg")==0)
		return 2;
	else if(strcmp(ext,".gif")==0)
		return 2;
	else if(strcmp(ext,".mov")==0)
		return 3;
	else if(strcmp(ext,".mkv")==0)
		return 3;
	else if(strcmp(ext,".avi")==0)
		return 3;
	else if(strcmp(ext,".mp4")==0)
		return 3;
	else if(strcmp(ext,".JPG")==0)
		return 2;
	else if(strcmp(ext,".mp3")==0)
		return 4;
	else
		return 1;	
}

char servAddr[100];
DIR* dirp;
struct dirent* direntp;
GtkBuilder *builder;
GtkWidget  *applicationwindow;
char globalname[3000];
bool startjoin=true; 
char msg[10];
int recvcnt=0;
int itisjoin=0;
GtkFileChooserButton *fileChooserButton;
pthread_t serverthread;
int isstart=0,iscreate=0,issend=0,isend=0,isrecv=0,mulre=0,mulse=0;
int fpp;
FILE *fp;
char recvfile[256];
int sock,secsock,newsock,socke,secnewsock,secsocke;
long long int sizerecv=0;
long long int sizesend;
long long int size=0;
GtkWidget *newImageBlock(char *, int );
GtkWidget *sendBox;
GtkWidget *recvBox;
GtkWidget *imageBox;
int createMode=0,joinMode=0,sendMode=0;
pthread_t cliethread;
pthread_t servethread;
pthread_t clientthread;
pthread_t serverthread;

struct psnode{
	GtkWidget *button;
	char ip[100];
	struct psnode *ptr;
}*phead=NULL;

struct node{
	char filename[100];
	int type;
	struct node *ptr;
}*head;

struct selectnode
{
	char filename[100];
    char foldername[3000];
	struct selectnode * ptr;
}*selected,*sending,*sendend;

void closeConnection(){
	if(iscreate){
		close(secnewsock);
		close(newsock);
   		close(sock);
	}
	else{
		close(socke);
		close(secsocke);
	}
}

void initialize(){
	startjoin=true; 
	isstart=0;
	iscreate=0;
	issend=0;
	isend=0;
	isrecv=0;
	mulre=0;
	mulse=0;
	size=0;
	sizerecv=0;
	recvcnt=0;
	itisjoin=0;
	selected=NULL;
	sendend=NULL;
	sending=NULL;
	phead=NULL;
	head=NULL;
}

void selectpsnode(GtkWidget *button, gpointer user_data){
	struct psnode *temp=phead;
	while(temp->button!=button)
		temp=temp->ptr;
	strcpy(servAddr,temp->ip);
	startjoin=FALSE;
	Joinclicked();	
}

GtkWidget *newpsBlock(char *filename){
	GtkWidget  *button;
	GtkWidget  *box;
	GtkWidget  *image;
	GtkWidget  *name;

	button=GTK_WIDGET(gtk_button_new());
	box=GTK_WIDGET(gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0));
	GdkPixbuf *pixbuf;
	pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile3.png", -1, 50, NULL);
	image=gtk_image_new_from_pixbuf(pixbuf);
	name=gtk_label_new(filename);
	gtk_box_pack_start(GTK_BOX(box),image,0,0,0);
	gtk_box_pack_start(GTK_BOX(box),name,0,0,0);
	gtk_container_add(GTK_CONTAINER(button),box);
	gtk_widget_show(button);
	gtk_widget_show(image);
	gtk_widget_show(name);	
	gtk_widget_show(box);
	gtk_widget_set_size_request(button,100,-1);
	g_signal_connect(button,"clicked", G_CALLBACK(selectpsnode),NULL);
	return button;
}

void startPortScanner(){
	g_print("IN port Scanner function\n");
	char ip[20];
	char command[100];
	char buffer[256];
		
	struct hostent *host;
	int err,psock,start,end;
	struct sockaddr_in sa;	
	strncpy((char*)&sa , "" , sizeof sa);
	sa.sin_family = AF_INET;	
	sa.sin_port = htons(5011);		
	psock = socket(AF_INET , SOCK_STREAM , 0);
	if(psock < 0){
		g_print("\nCan't open Socket");
	}	
	GtkWidget *psgrid=GTK_WIDGET(gtk_builder_get_object(builder,"box6"));
	char ipbase[15]="192.168.0.";
	int i;
	for(i=1;i<4;i++){
		while(gtk_events_pending())
			gtk_main_iteration();
		sprintf(ip,"%s%d",ipbase,i);
		g_print("%s..\n",ip);	
		sa.sin_addr.s_addr = inet_addr(ip);
		err = connect(psock,(struct sockaddr*)&sa , sizeof sa);
		if(err < 0){
			g_print("Can't connect to %s\n",ip);
		}
		else{
			g_print("%s open\n",ip);
            
			if(recv(psock,buffer,256,0)<0)
			{
				perror("recvnotok");
        		printf("errno = %d.\n", errno);
			}
			else
			{
				g_print("\nReceived name : %s",buffer);
			}

			FILE * portfile=fopen("Interfaces/profile3.png","w");
			char buf[256];
			long long int len;
			long long int remain_data;
			recv(psock,buf,256,0);
			remain_data=atoll(buf);
			g_print("\nThe image size : %lld\n",remain_data);
			while ((remain_data > 0) && ((len = recv(psock, buf,256,0)) > 0))
        	{	 	
        	    fwrite(buf, sizeof(char), len, portfile);
        	    remain_data -= len;
				g_print("%d\n",len);
			}
			bzero(buf,0);
			strcpy(buf,"0");
			send(psock,buf,strlen(buf),0);
			fclose(portfile);			


			GtkWidget *button=newpsBlock(buffer);
			struct psnode *temp=(struct psnode *)malloc(sizeof(struct psnode));
			temp->button=button;
			strcpy(temp->ip,ip);
			temp->ptr=NULL;			
			if(phead==NULL){	
				phead=temp;
			}
			else{
				temp->ptr=phead;
				phead=temp;
			}
			gtk_box_pack_start((GTK_BOX(psgrid)),button,0,0,0);
			close(psock);
			psock = socket(AF_INET , SOCK_STREAM , 0);
			if(psock < 0){
				g_print("\nCan't open Socket");
			}		
		}	
	}	
	close(psock);
}

void hidepswindow(){
	GtkWidget *psWindow=GTK_WIDGET(gtk_builder_get_object(builder,"window2"));
	gtk_widget_hide(psWindow);
}

void * sendputget();

void changefolder()
{
	char getname[300];
	int filetype;
	GtkWidget *button;

	dirp =opendir(globalname);
	GtkWidget  *grid1;
	grid1=GTK_WIDGET(gtk_builder_get_object(builder,"grid1"));
 
    if(dirp == NULL){
        perror("can't open folder");
    }else{
        direntp = readdir( dirp );
		int row=0,col=0;
		while(direntp!=NULL){
			strcpy(getname,direntp->d_name);
			filetype=direntp->d_type;
			direntp = readdir( dirp );		
			if(filetype!=4)
				continue;
			if(getname[0]=='.' || getname[(strlen(getname))-1]=='~')
				continue;
			button=newImageBlock(getname,filetype);
			gtk_style_context_add_class(gtk_widget_get_style_context(button),"settingButtons");
			gtk_grid_attach(GTK_GRID(grid1),button,col,row,1,1);
			if(col/5==1)
				row++;
			col=(col+1)%6;
		}
		closedir( dirp );
		dirp =opendir(globalname);
        direntp = readdir( dirp );
		while(direntp!=NULL){
			strcpy(getname,direntp->d_name);
			filetype=direntp->d_type;
			direntp = readdir( dirp );		
			if(filetype!=8)
				continue;
			if(getname[0]=='.' || getname[(strlen(getname))-1]=='~')
				continue;
			button=newImageBlock(getname,filetype);
			gtk_style_context_add_class(gtk_widget_get_style_context(button),"settingButtons");
			gtk_grid_attach(GTK_GRID(grid1),button,col,row,1,1);
			if(col/5==1)
				row++;
			col=(col+1)%6;
		}
		closedir( dirp );
    }
}
int checkselect(char * typename,int delete)
{
	struct selectnode * check,*back=NULL;
	check=selected;
	int selectedok=0;
	while(check!=NULL)
    {
		if(strcmp(check->filename,typename)==0 && strcmp(check->foldername,globalname)==0)
		{
			if(delete){
				if(back==NULL)
					selected=check->ptr;
				else
					back->ptr=check->ptr;
			}
			selectedok=1;
			break;
		}
		back=check;
		check=check->ptr;
	}
	return selectedok;
}

void selectIcon(GtkWidget *button, gpointer user_data){
	struct node * prev;
	GtkImageType imagenam;
	GtkWidget  *grid1;
    grid1=GTK_WIDGET(gtk_builder_get_object(builder,"grid1"));
	char namefile[100];
	GList *children=gtk_container_get_children(GTK_CONTAINER(button));
	children=gtk_container_get_children(GTK_CONTAINER(children->data));
	strcpy(namefile,gtk_widget_get_tooltip_text (button));
	prev=head;
	GList *children2, *iter;
	while(prev!=NULL)
    {
		if(strcmp(prev->filename,namefile)==0)
		{
			if(prev->type==4)
			{
				//g_print("%s is folder",namefile);
				children2 = gtk_container_get_children(GTK_CONTAINER(grid1));
				for(iter = children2; iter != NULL; iter = g_list_next(iter))
			  		gtk_container_remove(GTK_CONTAINER(grid1),GTK_WIDGET(iter->data));
				g_list_free(children2);
				strcat(globalname,"/");
				strcat(globalname,namefile);
				//g_print("globalname :%s",globalname);
				head=NULL;
				changefolder();
			}
			else
			{
				//g_print("%s is file",namefile);	
				int iconNumber;			
				while(children!=NULL){
					if(GTK_IS_IMAGE(children->data)){
						if(checkselect(namefile,1)){
							//g_print("fileicon\n");
							iconNumber=checkExtensions(namefile);
							switch(iconNumber){
								case 1:	gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/file-icon.png");
										break;
								case 2: gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/img-icon.png");
										break;
								case 3: gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/video-icon.png");
										break;	
								case 4: gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/mp3-icon.png");
										break;	
								
							}
						}else{
							//g_print("filetick\n");
							//gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/file-tick.png");
							iconNumber=checkExtensions(namefile);
							switch(iconNumber){
								case 1:	gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/file-tick.png");
										break;
								case 2: gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/img-tick.png");
										break;
								case 3: gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/video-tick.png");
										break;
								case 4: gtk_image_set_from_file (GTK_IMAGE(children->data),"Interfaces/mp3-tick.png");
										break;				
							}							
							struct selectnode * temp=(struct selectnode *)malloc(sizeof(struct selectnode)); 
	 						strcpy(temp->filename,namefile);
							//g_print("filetickend");
							strcpy(temp->foldername,globalname);
							temp->ptr=NULL;
							if(selected==NULL)
							{
								selected=temp;
							}
							else
							{
								temp->ptr=selected;
								selected=temp;
							}
						}
					}
				children=g_list_next(children);
				}
				
			}
            break;
		}
		else
		{
			prev=prev->ptr;
		}
	}
}
GtkWidget *newImageBlock(char *filename, int filetype)
{
	GtkWidget  *button;
	GtkWidget  *box;
	GtkWidget  *image;
	GtkWidget  *name;
	//g_print("filename :%s type :%d",filename,filetype);
	struct node * temp=(struct node *)malloc(sizeof(struct node));
    strcpy(temp->filename,filename);
	temp->type=filetype;
	if(head==NULL)
	{
		head=temp;
		head->ptr=NULL;
	}
    else
    {
    	temp->ptr=head;
   		head=temp;
    }
	button=GTK_WIDGET(gtk_button_new());
	box=GTK_WIDGET(gtk_box_new(GTK_ORIENTATION_VERTICAL,0));
	int iconNumber;
	if(filetype==8)	{
        if(checkselect(filename,0)){
			iconNumber=checkExtensions(filename);
			switch(iconNumber){
				case 1:	image=gtk_image_new_from_file("Interfaces/file-tick.png");
					break;
				case 2: image=gtk_image_new_from_file("Interfaces/img-tick.png");
					break;
				case 3: image=gtk_image_new_from_file("Interfaces/video-tick.png");
					break;	
				case 4: image=gtk_image_new_from_file("Interfaces/mp3-tick.png");
					break;				
			}
		}
		else{
			//image=gtk_image_new_from_file("Interfaces/file-icon.png");
			iconNumber=checkExtensions(filename);
			switch(iconNumber){
				case 1:	image=gtk_image_new_from_file("Interfaces/file-icon.png");
					break;
				case 2: image=gtk_image_new_from_file("Interfaces/img-icon.png");
					break;
				case 3: image=gtk_image_new_from_file("Interfaces/video-icon.png");
					break;	
				case 4: image=gtk_image_new_from_file("Interfaces/mp3-icon.png");
					break;				
			}
		}
	}
	else{
		image=gtk_image_new_from_file("Interfaces/folder-icon.png");
	}	
    gtk_widget_set_tooltip_text (button,filename);
	if(strlen(filename)>15)
	{
		filename[13]='.';
        filename[14]='.';
		filename[15]='.';
		filename[16]='\0';
	}
	name=gtk_label_new(filename);
	gtk_box_pack_start(GTK_BOX(box),image,0,0,0);
	gtk_box_pack_start(GTK_BOX(box),name,0,0,0);
	gtk_container_add(GTK_CONTAINER(button),box);
	//gtk_box_set_spacing (GTK_BOX(box),10);
	//gtk_button_clicked (button);
	gtk_widget_show(button);
	gtk_widget_show(image);
	gtk_widget_show(name);	
	gtk_widget_show(box);
	gtk_widget_set_size_request(button,120,-1);
	g_signal_connect(button,"clicked", G_CALLBACK(selectIcon),NULL);
	return button;
}
///////////////////////////////////////////////////////
//sendrecvstart

void * multiplesend()
{
 	
	int * offset;
    int sentbytes;
	offset=0;
    sizesend=size;
	g_print("mulsen size:%lld\n",sizesend);
	//GtkWidget *progress1=GTK_WIDGET(gtk_builder_get_object(builder,"button8"));
	//GtkWidget *sendBox=GTK_WIDGET(gtk_builder_get_object(builder,"box4"));
	//gtk_widget_show(sendBox);
	//gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progress1),0.00);
	if(itisjoin==0)
	{	
		g_print("itisjoin=0\n");
		sleep(1);
		double per;
		char str[10];
		while(((sentbytes=sendfile(newsock,fpp,&offset,5000000))>0) && (sizesend>0))
		{ 
			sizesend-= sentbytes;
			//per=(float)(total-sizesend)/total;
			//sprintf(str,"%.1lf",per);
			//per=atof(str);
			//g_print("%lf\n",per);
			//gtk_widget_set_size_request(progress1,(per*180),14);
			//gtk_progress_bar_pulse(progress1);
			//gtk_progress_bar_set_fraction(progress1,per);
			//gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progress1),per);
			//g_print("%d\n",sentbytes);
		}
	}
	else
	{
		sleep(1);
		double per;
		while(((sentbytes=sendfile(secsocke,fpp,&offset,5000000))>0) && (sizesend>0))
		{ 
			sizesend -= sentbytes;
			//per=(float)(total-sizesend)/total;
			//g_print("%lf\n",per);
			//g_print("%d\n",sentbytes);
			//gtk_progress_bar_set_fraction(progress1,per);
			//gtk_progress_bar_set_fraction(progress1,(float)(size-sizesend)/size);
		}
	}

	g_print("sendcomplete\n");
	//gtk_widget_hide(sendBox);
	issend=0;
	//gtk_progress_bar_set_fraction(progress1,(double)0.00);
}

void * multiplerecv(){

		char buffok[5000000];
		int len;
		long long int remain_data=0;	
		remain_data=sizerecv;
		g_print("mulrecv size:%lld\n",sizerecv);
		//gtk_widget_show(recvBox);
		//gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progress2),0.00);
		//GtkProgressBar * progress2=GTK_PROGRESS_BAR(gtk_builder_get_object(builder,"progressbar2"));
		fp=fopen(recvfile,"w");
        if(itisjoin==1)
		{
        	while ((remain_data > 0) && ((len = recv(socke, buffok,5000000,0)) > 0))
        	{	
                fwrite(buffok, sizeof(char), len, fp);
                remain_data -= len;
				g_print("%d\n",len);
				bzero(buffok,5000000);
				//gtk_progress_bar_set_fraction(progress2,(double)(sizerecv-remain_data)/sizerecv);
				//gtk_progress_bar_set_fraction(progress2,(float)(sizerecv-remain_data)/sizerecv);
        	}
		}
		else
		{
			while ((remain_data > 0) && ((len = recv(secnewsock, buffok,5000000,0)) > 0))
        	{	 	
                fwrite(buffok, sizeof(char), len, fp);
                remain_data -= len;
				g_print("%d\n",len);
				bzero(buffok,5000000);
				//gtk_progress_bar_set_fraction(progress2,(double)(sizerecv-remain_data)/sizerecv);
        		//gtk_progress_bar_set_fraction(progress2,(float)(sizerecv-remain_data)/sizerecv);
        	}

		}
        fclose(fp);
		g_print("recvcomplete");
		//gtk_widget_hide(recvBox);
		isrecv=0;
		//gtk_progress_bar_set_fraction(progress2,(double)0.00);
}

//sendrecvend
//////////////////////////////////////////////
//mulmoveselectstart

void * multipleselect()
{
    pthread_t getthread;
	char buffer[256];
 	int n;
	g_print("multipleselectfun:");
	char fileName[100];
	char buf[256];
	struct stat file_stat;
	while(isend==0)
 	{
		bzero(buffer,256);
		recv(newsock,buffer,256,0);
		if(strcmp(buffer,"get\n")==0)
		{	
			bzero(buffer,256);
			strcpy(buffer,"put\n");
			send(newsock,buffer,256,0);
			//pthread_create(&getthread,NULL,multiplerecv,NULL);	
			//pthread_join(getthread,NULL);	
		}
		else if(strcmp(buffer,"put\n")==0)
		{	 

			bzero(buffer,256);
     		//fileName=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fileChooserButton));
			sprintf(fileName,"%s/%s",sending->foldername,sending->filename);
			g_print("filename is:%s",fileName);
			char sfilename[100];
			GtkWidget *sLabel=GTK_WIDGET(gtk_builder_get_object(builder,"label2"));
			strcpy(sfilename,sending->filename);
			if(strlen(sfilename)>25){
				sfilename[23]='.';
		        sfilename[24]='.';
				sfilename[25]='.';
				sfilename[26]='\0';
			}
			gtk_label_set_text(GTK_LABEL(sLabel),sfilename);
			gtk_widget_show(sendBox);
    		strcpy(buffer,sending->filename);
			g_print("buffer is:%s",buffer);
    		send(newsock,buffer,strlen(buffer),0);
			bzero(buffer,256);
			fpp=open(fileName,O_RDONLY);
			fstat(fpp, &file_stat);
			size=file_stat.st_size;
			bzero(buf,256);
			sprintf(buf,"%lld",size);
			send(newsock,buf,strlen(buf),0);
			pthread_create(&serverthread,NULL,multiplesend,NULL);
			//sleep(1);
			pthread_join(serverthread,NULL);	
			gtk_widget_hide(sendBox);
			sending=sending->ptr;
			if(sending!=NULL)
			{
				//sendputget();
				pthread_create(&getthread,NULL,sendputget,NULL);
			}
		}


	}	

}

//////////////////////////////
void * secmultipleselect()
{
    pthread_t getthread;
	char buffer[256];
 	int n;
	char buf[256];
	g_print("entered secmultipleselect fun:\n");
	char fileName[100];
	struct stat file_stat;
	while(isend==0)
 	{
		g_print("Entered while loop ms\n");
		bzero(buffer,256);
		recv(secsocke,buffer,256,0);
		if(strcmp(buffer,"get\n")==0)
		{	
			bzero(buffer,256);
			strcpy(buffer,"put\n");
			send(secsocke,buffer,256,0);
			//pthread_create(&getthread,NULL,multiplerecv,NULL);	
			//pthread_join(getthread,NULL);	
		}
		else if(strcmp(buffer,"put\n")==0)
		{	 

			bzero(buffer,256);
     		//fileName=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fileChooserButton));
			sprintf(fileName,"%s/%s",sending->foldername,sending->filename);
			g_print("filename is:%s",fileName);
			GtkWidget *sLabel=GTK_WIDGET(gtk_builder_get_object(builder,"label2"));
			char sfilename[100];
			strcpy(sfilename,sending->filename);
			if(strlen(sfilename)>25){
				sfilename[23]='.';
		        sfilename[24]='.';
				sfilename[25]='.';
				sfilename[26]='\0';
			}
			gtk_label_set_text(GTK_LABEL(sLabel),sfilename);
			gtk_widget_show(sendBox);
    		strcpy(buffer,sending->filename);
			g_print("buffer is:%s",buffer);
    		send(secsocke,buffer,strlen(buffer),0);
			bzero(buffer,256);
			fpp=open(fileName,O_RDONLY);
			fstat(fpp, &file_stat);
			size=file_stat.st_size;
			bzero(buf,256);
			sprintf(buf,"%lld",size);
			send(secsocke,buf,strlen(buf),0);
			pthread_create(&serverthread,NULL,multiplesend,NULL);
			pthread_join(serverthread,NULL);
			gtk_widget_hide(sendBox);
			sending=sending->ptr;
			if(sending!=NULL)
			{
				//sendputget();
				pthread_create(&getthread,NULL,sendputget,NULL);
			}	
		}


	}	

}


/////////////////////////////////////////////
void * multiplemove()
{
    pthread_t putthread;
    char buffer[256];
 	int n;
	g_print("entered multiplemove fun:\n");
	int remain_data = 0;
	while(isend==0)
 	{
		g_print("Entered while loop\n");
		bzero(buffer,256);
		recv(socke,buffer,256,0);
		g_print("\nBuffer received is %s\n",buffer);
		if(strcmp(buffer,"get\n")==0)
		{	
			bzero(buffer,256);
			strcpy(buffer,"put\n");
			send(socke,buffer,256,0);	
			bzero(buffer,256);
			recv(socke,buffer,256,0);
			//readonlyfilename(buffer);
			strcpy(recvfile,buffer);
			GtkWidget *sLabel=GTK_WIDGET(gtk_builder_get_object(builder,"label3"));
			char sfilename[100];
			strcpy(sfilename,recvfile);
			if(strlen(sfilename)>25){
				sfilename[23]='.';
		        sfilename[24]='.';
				sfilename[25]='.';
				sfilename[26]='\0';
			}
			gtk_label_set_text(GTK_LABEL(sLabel),sfilename);
			gtk_widget_show(recvBox);
			bzero(buffer,256);
			recv(socke,buffer,256,0);
			sizerecv=atoll(buffer);
			pthread_create(&putthread,NULL,multiplerecv,NULL);	
			pthread_join(putthread,NULL);
			gtk_widget_hide(recvBox);
		}
		else if(strcmp(buffer,"put\n")==0)
		{	
			//pthread_create(&serverthread,NULL,multiplesend,NULL);	
			//pthread_join(serverthread,NULL);
		}


	}	

}
////////////////////////////////

void * secmultiplemove()
{
    pthread_t putthread;
    char buffer[256];
 	int n;
	g_print("secmultiplemovefun:");
	int remain_data = 0;
	while(isend==0)
 	{
		bzero(buffer,256);
		recv(secnewsock,buffer,256,0);
		//g_print("\nrecvstart:");
		if(strcmp(buffer,"get\n")==0)
		{	
			bzero(buffer,256);
			strcpy(buffer,"put\n");
			send(secnewsock,buffer,256,0);	
			bzero(buffer,256);
			recv(secnewsock,buffer,256,0);
			//readonlyfilename(buffer);
			strcpy(recvfile,buffer);
			bzero(buffer,256);
			recv(secnewsock,buffer,256,0);
			sizerecv=atoll(buffer);
			GtkWidget *sLabel=GTK_WIDGET(gtk_builder_get_object(builder,"label3"));
			char sfilename[100];
			strcpy(sfilename,recvfile);
			if(strlen(sfilename)>25){
				sfilename[23]='.';
		        sfilename[24]='.';
				sfilename[25]='.';
				sfilename[26]='\0';
			}
			gtk_label_set_text(GTK_LABEL(sLabel),sfilename);
			gtk_widget_show(recvBox);
			pthread_create(&putthread,NULL,multiplerecv,NULL);	
			pthread_join(putthread,NULL);
			gtk_widget_hide(recvBox);
		}
		else if(strcmp(buffer,"put\n")==0)
		{	
			//pthread_create(&serverthread,NULL,multiplesend,NULL);	
			//pthread_join(serverthread,NULL);
		}


	}	

}

//mulmoveselectend
//////////////////////////////////////////////

///sendclicked

void * sendputget()
{
	if(sending!=NULL)
	{
		//issend=1;
		char buffer[256];
		bzero(buffer,256);
		strcpy(buffer,"get\n");
		g_print("send clicked\n");
		sleep(1);
		if(itisjoin==0)
			send(newsock,buffer,strlen(buffer),0);
		else
			send(secsocke,buffer,strlen(buffer),0);
		g_print("send clicked end\n");
    }


}

void Sendclicked()
{
	if(sendMode==0)
		return;
	GList *children2,*iter;
	GtkWidget *grid1;
	grid1=GTK_WIDGET(gtk_builder_get_object(builder,"grid1"));
	g_print("***Entering function ***\n");
	if(sending==NULL)
	{
		sending=selected;
		pthread_t ppthread;
		//sendend=selected;
		int endprint=0;
		while(selected!=NULL)
		{
			sendend=selected;
			selected=selected->ptr;
			endprint++;
		}
		g_print("sendend:%s\n",sendend->filename);
		g_print("sendend:%d\n",endprint);
		selected=NULL;
		pthread_create(&ppthread,NULL,sendputget,NULL);
		//sendputget();
	}
	else
	{
		int endpprint=0;
		while(selected!=NULL)
		{
			sendend->ptr=selected;
			sendend=sendend->ptr;
			selected=selected->ptr;
			endpprint++;
		}
		g_print("sendend:%d\n",endpprint);
		g_print("sendend:%s\n",sendend->filename);
	}
	children2 = gtk_container_get_children(GTK_CONTAINER(grid1));
	for(iter = children2; iter != NULL; iter = g_list_next(iter))
		gtk_container_remove(GTK_CONTAINER(grid1),GTK_WIDGET(iter->data));
	g_list_free(children2);
	changefolder();

	/*struct selectnode * sendprev;
	int endppprint=0;
	sendprev=sending;
	while(sendprev!=NULL)
	{
		sendprev=sendprev->ptr;
		endppprint++;
	}
	g_print("sendend:%d\n",endppprint);*/
	g_print("***Leaving function ***\n");
}
//////////////////////////////////////////////////////////////////////
//joinandcreate
void Joinclicked(GtkWidget *button, gpointer user_data){
	if(joinMode==-1)
		return;
	else if(joinMode==1){
		//code to close connection
		joinMode=0;
		createMode=0;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(builder,"image3"));
		gtk_image_set_from_file(img,"Interfaces/start.png");
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image4"));
		gtk_image_set_from_file(img,"Interfaces/join.png");
		
		sendMode=0;
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image5"));
		gtk_image_set_from_file(img,"Interfaces/send-dim.png");
		gtk_widget_hide(imageBox);
		pthread_cancel(clientthread);
		pthread_cancel(serverthread);		
		closeConnection();
		initialize();
		return;
	}

	if(startjoin){
		GtkWidget *psWindow=GTK_WIDGET(gtk_builder_get_object(builder,"window2"));
		gtk_widget_show(psWindow);
		while(gtk_events_pending())
			gtk_main_iteration();
		startPortScanner();

		if(iscreate==0){
			itisjoin=1;
		}
	}
	else{
		hidepswindow();
		startjoin=true;

		int portno,n;
		socklen_t clilen;
		struct sockaddr_in serv;
		struct hostent *server;

		char buffer[256];
		bzero((char*)&serv,sizeof(serv));
		socke=socket(AF_INET,SOCK_STREAM,0);
		secsocke=socket(AF_INET,SOCK_STREAM,0);
		portno=5011;
		server=gethostbyname(servAddr);
		serv.sin_family=AF_INET;
		bcopy((char *)server->h_addr,(char *)&serv.sin_addr.s_addr,server->h_length);
		serv.sin_port=htons(portno);
		if(connect(socke,(struct sockaddr *)&serv,sizeof(serv))<0){
			g_print("client not okok");
		}
		else{
			g_print("Connection Success \n");
		}

		char recName[100];
		bzero(buffer,256);
		if(recv(socke,buffer,256,0)<0)
		{
			perror("recvnotok");
       		printf("errno = %d.\n", errno);
		}
		else
		{
			g_print("Name Received : %s\n",buffer);
			strcpy(recName,buffer);
		}
		FILE * portfile=fopen("Interfaces/profile3.png","w");
		char buf[256];
		bzero(buf,256);
		long long int len;
		long long int remain_data;
		recv(socke,buf,256,0);
		remain_data=atoll(buf);
		g_print("The file size is %lld\n",remain_data);
		while ((remain_data > 0) && ((len = recv(socke, buf,256,0)) > 0))
       	{	 	
       	    fwrite(buf, sizeof(char), len, portfile);
       	    remain_data -= len;
			g_print("%d\n",len);
		}
		bzero(buf,256);
		strcpy(buf,"1");
		send(socke,buf,strlen(buf),0);
		fclose(portfile);
		bzero(buffer,256);
//
		if(connect(secsocke,(struct sockaddr *)&serv,sizeof(serv))<0)
    	{
			g_print("client not okok");
		}
		else
		{
			g_print("Second Connection Done\n");
		}
		FILE *fpm=fopen("profileNode","r");
		struct profileConfig *hNode=(struct profileConfig *)malloc(sizeof(struct profileConfig));
		fread(hNode,sizeof(struct profileConfig),1,fpm);
		strcpy(buffer,hNode->name);
		fclose(fpm);
	    sleep(1);
		if(send(socke,buffer,strlen(buffer),0)<0)
    	{
  			g_print("send not okk");
    	}
		else
		{
			g_print("Send Name Done\n");
		}
		
		int filestart;
		long long int sentbytes;
		long long int sizesend;
		filestart=open("Interfaces/profile.png",O_RDONLY);
		struct stat file_stat;
		fstat(filestart, &file_stat);
		size=file_stat.st_size;
		bzero(buf,256);
		sprintf(buf,"%lld",size);
		sleep(1);
		send(socke,buf,strlen(buf),0);
		g_print("Size send : %lld\n",size);
		sizesend=size;
		int *offset=0;
		sleep(1);
       	while(((sentbytes=sendfile(socke,filestart,&offset,256))>0) && (sizesend>0)){ 
			sizesend-= sentbytes;
		}
		close(filestart);

		GtkLabel *Clientname=GTK_LABEL(gtk_builder_get_object(builder,"label1"));
		gtk_label_set_text (Clientname,recName);
		g_print("Set Label name\n");
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(builder,"image7"));
		GdkPixbuf *pixbuf;
		pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile3.png", -1, 50, NULL);
		gtk_image_set_from_pixbuf(img,pixbuf);
		gtk_widget_show(imageBox);

		pthread_create(&clientthread,NULL,multiplemove,NULL);
		pthread_create(&serverthread,NULL,secmultipleselect,NULL);
	
		sendMode=1;
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image5"));
		gtk_image_set_from_file(img,"Interfaces/send-button.png");
		joinMode=1;
		createMode=-1;
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image3"));
		gtk_image_set_from_file(img,"Interfaces/start-dim.png");
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image4"));
		gtk_image_set_from_file(img,"Interfaces/stop.png");

	}
}
gboolean imageTooltip1(GtkStatusIcon *status_icon, gint x, gint y, gboolean keyboard_mode, GtkTooltip *tooltip, gpointer user_data){
	
	GdkPixbuf *pixbuf;
	pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile3.png", -1, 300, NULL);
	GtkWidget *bigImage=gtk_image_new();
	gtk_image_set_from_pixbuf(GTK_IMAGE(bigImage),pixbuf);
	gtk_widget_show(bigImage);
	gtk_tooltip_set_custom(tooltip,bigImage);
}
void *create_clicked(){
	iscreate=1;
   	int portno,secportno,n;
	socklen_t clilen;
	socklen_t secclilen;
	struct sockaddr_in serv,clien,secclien;
	char buffer[256];
	bzero((char*)&serv,sizeof(serv));
	if((sock=socket(AF_INET,SOCK_STREAM,0))<0){
		g_print("Socket creation failed\n");
	}
	else{
		g_print("Socket Creation success\n");
	}
	portno=5011;
	serv.sin_family=AF_INET;
	serv.sin_addr.s_addr=INADDR_ANY;
	serv.sin_port=htons(portno);
	if(bind(sock,(struct sockaddr *)&serv,sizeof(serv))<0){
		g_print("Binding failed\n");
	}
	else{
		g_print("Binding Done\n");
	}
	clilen=sizeof(clien);
	secclilen=sizeof(secclien);
	listen(sock,10);
   	
	int filestart;
	char buf[256];
	long long int sentbytes;
	long long int sizesend;
	while(1){
		if((newsock=accept(sock,(struct sockaddr *)&clien,&clilen))<0){
			g_print("Accepting failed\n");
		}
		else{
			g_print("Connection Done\n");
		}
	    FILE *fpm=fopen("profileNode","r");
		struct profileConfig *hNode=(struct profileConfig *)malloc(sizeof(struct profileConfig));
		fread(hNode,sizeof(struct profileConfig),1,fpm);
		strcpy(buffer,hNode->name);
		fclose(fpm);
		if(send(newsock,buffer,strlen(buffer),0)<0){
			g_print("Sending profile name failed\n");
		}
		else{
			g_print("Sent profile name\n");
		}


			
			filestart=open("Interfaces/profile.png",O_RDONLY);
			struct stat file_stat;
			fstat(filestart, &file_stat);
			size=file_stat.st_size;
			bzero(buf,256);
			sprintf(buf,"%lld",size);
			g_print("Image size is %lld\n",size);
			sleep(1);
			send(newsock,buf,strlen(buf),0);
			sizesend=size;
			int *offset=0;
			sleep(1);
        	while(((sentbytes=sendfile(newsock,filestart,&offset,256))>0) && (sizesend>0))
			{ 
				sizesend-= sentbytes;
			}
			close(filestart);

			bzero(buffer,256);
			recv(newsock,buffer,10,0);
			if(strcmp(buffer,"0")==0)
			{
				g_print("Requested to close the connection\n");
				close(newsock);	
			}
			else
       			break;
		}
//

		if((secnewsock=accept(sock,(struct sockaddr *)&secclien,&secclilen))<0)
		{
			g_print("notokoko");
		}
		else
			g_print("connectokok");
		bzero(buffer,256);
		if(recv(newsock,buffer,256,0)<0)
		{
			perror("recvnotok");
        	printf("errno = %d.\n", errno);
		}
		else
		{
			g_print("Recv okk");
		}
	recv(newsock,buf,256,0);

	long long int remain_data=atoll(buf);
	int len;
	FILE * recvfile =fopen("Interfaces/profile3.png","w");
	while ((remain_data > 0) && ((len = recv(newsock, buf,256,0)) > 0)){	 	
        fwrite(buf, sizeof(char), len, recvfile);
        remain_data -= len;
		g_print("%d\n",len);			
   	}
	fclose(recvfile);

	pthread_create(&cliethread,NULL,multipleselect,NULL);
	pthread_create(&servethread,NULL,secmultiplemove,NULL);

	sendMode=1;
	GtkImage *img=GTK_IMAGE(gtk_builder_get_object(builder,"image5"));
	gtk_image_set_from_file(img,"Interfaces/send-button.png");				
		
	GtkLabel *Clientname=GTK_LABEL(gtk_builder_get_object(builder,"label1"));
	gtk_label_set_text (Clientname,buffer);

	img=GTK_IMAGE(gtk_builder_get_object(builder,"image7"));
   	GdkPixbuf *pixbuf;
	pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile3.png", -1, 50, NULL);
	gtk_image_set_from_pixbuf(img,pixbuf);
	gtk_widget_show(imageBox);
}
void Createclicked(GtkWidget *button, gpointer user_data){
	if(createMode==0){
		joinMode=-1;
		createMode=1;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(builder,"image3"));
		gtk_image_set_from_file(img,"Interfaces/stop.png");
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image4"));
		gtk_image_set_from_file(img,"Interfaces/join-dim.png");
	}
	else if(createMode==1){
		joinMode=0;
		createMode=0;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(builder,"image3"));
		gtk_image_set_from_file(img,"Interfaces/start.png");
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image4"));
		gtk_image_set_from_file(img,"Interfaces/join.png");
		
		sendMode=0;
		img=GTK_IMAGE(gtk_builder_get_object(builder,"image5"));
		gtk_image_set_from_file(img,"Interfaces/send-dim.png");
		gtk_widget_hide(imageBox);
		pthread_cancel(cliethread);
		pthread_cancel(servethread);		
		closeConnection();
		initialize();
		return;
	}
	else
		return;

	pthread_t dynamiccreate;
	pthread_create(&dynamiccreate,NULL,create_clicked,NULL);
}
///////////////////////////////////////////////////////////////////////////
void Backclicked()
{
    int i=0;
	GList *children2, *iter;
	GtkWidget  *grid1;
	grid1=GTK_WIDGET(gtk_builder_get_object(builder,"grid1"));
	char str[100];
	FILE *fn;
	fn=popen("who | awk \'NR==1{ print $1 \'}","r");
	fgets(str,40,fn);
	str[strlen(str)-1]='\0';
	pclose(fn);
	sprintf(str,"/home/%s",str);
	if(strcmp(globalname,str)==0)
  	 	return ;
 	for(i=strlen(globalname)-1;i>0;i--)
    {
      	if(globalname[i]=='/')
  		{
			globalname[i]='\0';
			break;
		} 
    }
	children2 = gtk_container_get_children(GTK_CONTAINER(grid1));
	for(iter = children2; iter != NULL; iter = g_list_next(iter))
  		gtk_container_remove(GTK_CONTAINER(grid1),GTK_WIDGET(iter->data));
	g_list_free(children2);
	head=NULL;
	changefolder();
}
void Homeclicked()
{
	g_print("Reached home clicked function");
	FILE *fn;
	char str[30];
	fn=popen("who | awk \'NR==1{ print $1 \'}","r");
	fgets(str,30,fn);
	str[strlen(str)-1]='\0';
	g_print("%s..\n",str);
	pclose(fn);
    sprintf(globalname,"/home/%s",str);
	g_print("The folder name is %s\n",globalname);
    GList *children2, *iter;
	GtkWidget  *grid1;
	grid1=GTK_WIDGET(gtk_builder_get_object(builder,"grid1"));
	children2 = gtk_container_get_children(GTK_CONTAINER(grid1));
	for(iter = children2; iter != NULL; iter = g_list_next(iter))
  		gtk_container_remove(GTK_CONTAINER(grid1),GTK_WIDGET(iter->data));
	g_list_free(children2);
	head=NULL;
	changefolder();
}

void ftp_window_show(){
	GError *error=NULL;
//	g_print("Working...\n");
    builder = gtk_builder_new();
   	if(!gtk_builder_add_from_file(builder,"Interfaces/fileshare.glade",&error)){
        g_warning("%s",error->message);
        //g_free(error);
        return;
    }
	initialize();
	GtkWidget *darea = gtk_drawing_area_new();

	gtk_container_add(GTK_CONTAINER(gtk_builder_get_object(builder,"fs_icon_button")), darea);

  	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_fs_draw_event), NULL);  
	gtk_widget_set_size_request(darea,48,48);	
	gtk_style_context_add_class(gtk_widget_get_style_context(darea),"profileArea");
	gtk_widget_show_all(darea);	

	GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"ftp_button_image"));
	gtk_image_set_from_file(img,"Interfaces/ftp.png");

	applicationwindow = GTK_WIDGET( gtk_builder_get_object( builder, "window1" ) );
    gtk_builder_connect_signals( builder, NULL );
	GtkWidget *grid1=GTK_WIDGET(gtk_builder_get_object(builder,"grid2"));
	GtkWidget *swindow=	GTK_WIDGET(gtk_builder_get_object(builder,"scrolledwindow1"));
	GtkWidget *vport=	GTK_WIDGET(gtk_builder_get_object(builder,"paned1"));
	gtk_container_add(GTK_CONTAINER(swindow),vport);
	gtk_container_add(GTK_CONTAINER(grid1),swindow);

    Homeclicked();
		
	gtk_widget_set_size_request(applicationwindow,750,450);

    gtk_widget_show(applicationwindow);

	gtk_builder_connect_signals(builder,NULL);

	sendBox=GTK_WIDGET(gtk_builder_get_object(builder,"box4"));
	recvBox=GTK_WIDGET(gtk_builder_get_object(builder,"box5"));
	imageBox=GTK_WIDGET(gtk_builder_get_object(builder,"box10"));
	//progress1=GTK_WIDGET(gtk_builder_get_object(builder,"progressbar1"));
	//progress2=GTK_WIDGET(gtk_builder_get_object(builder,"progressbar2"));

	gtk_widget_hide(sendBox);
	gtk_widget_hide(recvBox);
	gtk_widget_hide(imageBox);
}
void ftp_exit(){
	gtk_widget_destroy(applicationwindow);
	GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"ftp_button_image"));
	gtk_image_set_from_file(img,"Interfaces/ftp1.png");
	isend=1;
	closeConnection();
	g_object_unref(G_OBJECT(builder));
}
