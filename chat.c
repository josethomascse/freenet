#include "main.h"
#include "chat.h"


#define def_host_name "localhost"
#define def_user_name "root"
#define def_password "chathan123"
#define def_db_name "freenet_sample"

// ----------function Declarations-----------

void start_chat_session(const char *username);
void registration_successfull();
//-------------------------------------------

GtkWidget  *chat_account_window;
GtkWidget  *create_account_window;
GtkWidget *alert_window;
GtkWidget *container;
GList *children, *iter;

static int is_account_created = 0;

// ----------------------------------mysql-------------------------------------------------
MYSQL *cnx_init;
MYSQL *cnx_db;
MYSQL_RES *result_set;
MYSQL_ROW row;
unsigned int ctr;


void show_result_set (MYSQL_RES *in_result_set);
void connect_to_database(){
	cnx_init = mysql_init (NULL);
	if (cnx_init == NULL){ 
		g_printf("failure in mysql_init\n");
		g_printf("Exit code 1\n");
		//exit(1);
	}
	
	cnx_db = mysql_real_connect ( cnx_init,def_host_name,def_user_name,def_password,def_db_name,0,NULL,0) ;
	if (cnx_db == NULL) {
		g_printf("failure in mysql_real_connect\n");
		g_printf("Exit code 2\n");
		g_printf("Error %u -- %s\n",mysql_errno (cnx_init),mysql_error (cnx_init));
		//exit(2);
	}
	else 
		g_printf("Connected to database\n");
	
	
}


void register_user(){

	connect_to_database();
	char query[200];
	const char *username,*password,*aliasname;
	//g_print("Helo");
	GtkEntry *user=GTK_ENTRY(gtk_builder_get_object(chatbuilder,"username_entry"));
	username=gtk_entry_get_text(user);
	GtkEntry *pass=GTK_ENTRY(gtk_builder_get_object(chatbuilder,"password_entry"));
	password=gtk_entry_get_text(pass);
	GtkEntry *alias=GTK_ENTRY(gtk_builder_get_object(chatbuilder,"aliasname_entry"));
	aliasname=gtk_entry_get_text(alias);

	sprintf(query,"insert into register values ('','%s','%s','%s');",username,password,aliasname);
	if(mysql_query(cnx_init,query)!=0)
		g_print("Registration Unsuccessfull\n");
	else{
		g_print("Registration successfull\n");
		create_account_window = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"create_account_window"));
		gtk_widget_hide(GTK_WIDGET(create_account_window));
		
		alert_window = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"alert_window"));

		GtkWidget *alert_container_grid = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"alert_container_grid"));
		children = gtk_container_get_children(GTK_CONTAINER(alert_container_grid));
		for(iter = children; iter != NULL; iter = g_list_next(iter))
		  	gtk_container_remove(GTK_CONTAINER(alert_container_grid),GTK_WIDGET(iter->data));
		GtkWidget *reg_success_grid=GTK_WIDGET(gtk_builder_get_object(chatbuilder,"reg_success_grid"));
		gtk_container_add(GTK_CONTAINER(alert_container_grid),reg_success_grid);
	
		gtk_builder_connect_signals(chatbuilder,NULL);
		gtk_widget_show(alert_window);
	}	
		
	
	//sprintf(query,"insert into register values ('','%s','%s','%s');",username,password,aliasname);
	mysql_query(cnx_init,"select * from register;");
	result_set = mysql_store_result (cnx_init);
	if (result_set == NULL)
		printf("failure in mysql_store_result for Select statement\n");
	else
		show_result_set (result_set);
}


void show_result_set (MYSQL_RES *in_result_set)
{
	while ((row = mysql_fetch_row (in_result_set)) != NULL)
	{
		for (ctr=0;ctr < mysql_num_fields (in_result_set); ctr++)
		{
			if ( ctr > 0 )
				fputc ('\t', stdout);
			g_printf ("%s",row[ctr]!=NULL?row[ctr]:"Null-val");
		}
		fputc ('\n', stdout);
	}
	if (mysql_errno (cnx_init) != 0)
	{
	g_printf ("failure in mysql_fetch_row\n");
	g_printf ("Exit code 3\n");
	g_printf("Error %u -- %s\n",
	mysql_errno (cnx_init),
	mysql_error (cnx_init));
	exit (3);
	}
	mysql_free_result (in_result_set);
}

//---------------------------------------------------------------------------------------


void create_account(){
	
	GtkWidget *chatwindow = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"create_account_window"));
	gtk_widget_show(chatwindow);
	gtk_widget_show_all( chat_account_window );
}


void open_chat_window(){
	
	GtkWidget *chatwindow = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_account"));
	gtk_widget_show(chatwindow);
	gtk_widget_show_all(chat_account_window );
}

/*void change_chat_image(){
	GtkWidget *chatimage = GTK_WIDGET(gtk_builder_get_object(builder,"chat_image"));
	chatimage = gtk_image_new_from_file ("1.png");
    gtk_container_add (GTK_CONTAINER (mainwindow), GTK_WIDGET (chatimage));
	gtk_widget_show_all (GTK_WIDGET (mainwindow));
	
}

void close_window(GtkWidget *widget, gpointer window_to_close)
{
    gtk_widget_destroy(GTK_WIDGET(window_to_close));
}*/


void chat_window_show(){
    
    GtkWidget *chat_account_create_grid,
			  *chat_main_grid;	
	GError *error=NULL;
	g_print("Working...\n");
    chatbuilder = gtk_builder_new();
   	if(!gtk_builder_add_from_file(chatbuilder,"Interfaces/chatActivity.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return;
    }
    chat_account_window = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_account_window"));	
	GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"chat_button_image"));
	gtk_image_set_from_file(img,"Interfaces/chat.png");
   
	/* if(is_account_created==0){
	g_print("Added\n");
    container = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_account_container_grid"));	
    /*if(!gtk_builder_add_from_file(tempBuilder,"Interfaces/mainWindow.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return;
    }*/
/*	children = gtk_container_get_children(GTK_CONTAINER(container));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(container),GTK_WIDGET(iter->data));
	chat_account_create_grid=GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_account_create_grid"));
	gtk_container_add(GTK_CONTAINER(container),chat_account_create_grid);
	//gtk_builder_connect_signals(mainBuilder,NULL);
	} */
	container = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_account_container_grid"));	
    /*if(!gtk_builder_add_from_file(tempBuilder,"Interfaces/mainWindow.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return;
    }*/
	children = gtk_container_get_children(GTK_CONTAINER(container));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(container),GTK_WIDGET(iter->data));
	chat_main_grid=GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_main_grid"));
	gtk_container_add(GTK_CONTAINER(container),chat_main_grid);
	
	gtk_builder_connect_signals(chatbuilder,NULL);
    gtk_widget_show(chat_account_window);
	
   // gtk_main();

    //return 0;
}

void add_username(){
	const char *username;
	GtkEntry *add_username=GTK_ENTRY(gtk_builder_get_object(chatbuilder,"add_username_entry"));
	username=gtk_entry_get_text(add_username);
	g_print("User %s added\n",username);
	start_chat_session(username);
}


void start_chat_session(const char *username){

    GtkWidget *chat_session_grid;
	GtkLabel *session_username;

	g_print("New session started with %s\n",username);
	container = GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_account_container_grid"));	
    /*if(!gtk_builder_add_from_file(tempBuilder,"Interfaces/mainWindow.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return;
    }*/
	children = gtk_container_get_children(GTK_CONTAINER(container));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(container),GTK_WIDGET(iter->data));
	chat_session_grid=GTK_WIDGET(gtk_builder_get_object(chatbuilder,"chat_session_grid"));
	gtk_container_add(GTK_CONTAINER(container),chat_session_grid);



	session_username = GTK_LABEL(gtk_builder_get_object(chatbuilder,"chat_session_username_label"));
	gtk_label_set_text (session_username,username);

		
}

void chat_exit(){
	gtk_widget_hide(chat_account_window);
	GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"chat_button_image"));
	gtk_image_set_from_file(img,"Interfaces/chat1.png");
	
}

