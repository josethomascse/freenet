CC = gcc
INCLUDES = -I/usr/include/mysql
LIBS = -L/usr/include/mysql -L/usr/lib/mysql -lmysqlclient -lz
CFLAGS =`pkg-config --libs --cflags gtk+-3.0` -rdynamic -pthread
SRC = $(wildcard *.c)

OBJS = $(SRC:.c=.o)

all: freenet

%.o: %.c
	$(CC) -c $(INCLUDES) -o $(@F) $(CFLAGS) $<

freenet: $(OBJS)
	$(CC) -o $(@F) $(OBJS) $(CFLAGS) $(LIBS)

clean:
	rm -f $(OBJS)
	rm -f freenet
