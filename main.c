#include "main.h"
#include "chat.h"
#include "ftp.h"
#include<pthread.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
struct{
	cairo_surface_t *image;
}glob;
pthread_t thread;
void loadProfileImage(cairo_t *cr);
gboolean on_draw_event(GtkWidget *widget,cairo_t *cr,gpointer user_data){      
 	loadProfileImage(cr);
  	return FALSE;
}
void loadProfileImage(cairo_t *cr){
	GdkPixbuf *pixbuf;

	pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile.png", -1, 50, NULL);
	glob.image = gdk_cairo_surface_create_from_pixbuf(pixbuf,0,NULL);
  	cairo_set_source_surface(cr, glob.image, 1, 1);
  	cairo_arc(cr,26, 26, 25, 0, 2*M_PI);
  	cairo_clip(cr);
  	cairo_paint(cr); 
 	g_object_unref (pixbuf);
}
void gtkExit(GtkWidget *widget,gpointer data){
//Function called at the time of exit
	if(connected)
		wifiConnect(NULL,NULL);

	if(hotspotStatus)
		stopHotspot();

	if(ftpStatus)
		ftpAction(NULL,NULL);

	g_object_unref(G_OBJECT(mainBuilder));
	cairo_surface_destroy(glob.image);
	g_object_unref(G_OBJECT(wifiBuilder));
	g_object_unref(G_OBJECT(hotspotBuilder));
	gtk_main_quit();

	FILE *fp;
	char str[100];
	fp=popen("sudo ifconfig freenet0 | grep bytes | awk \'{ print $2 \'} | cut -c7-","r");
	fgets(str,30,fp);	
	long long int ustr;
	ustr=atoll(str);
	ustr/=(1024*1024);
	pclose(fp);	
		
	fp=fopen("wifiUsage","r");
	fgets(str,30,fp);
	ustr+=atoll(str);
	sprintf(str,"%lld",ustr);
	fclose(fp);

	fp=fopen("wifiUsage","w");
	fputs(str,fp);
	fclose(fp);

	system("sudo ifconfig freenet0 down");
	system("sudo iw dev freenet0 del");
	system("sudo ifconfig freenet1 down");
	system("sudo iw dev freenet1 del");
	sprintf(str,"sudo ifconfig %s up",ifcName);
	system(str);
}
void *createEnvironment(){
	FILE *fptr;
  	fptr = popen("ls /sys/class/net | grep w", "r");
	fgets(ifcName,100,fptr);
	ifcName[strlen(ifcName)-1]='\0';
  	pclose(fptr);

	char command[100];

	system("sudo service network-manager stop");
	system("sleep 1");
	system("sudo pkill -15 nm-applet");
	system("sleep 1");
	sprintf(command,"sudo ifconfig %s down",ifcName);
	system(command);
	system("sleep 1");
	system("sudo iw phy phy0 interface add freenet0 type managed");
	system("sudo iw phy phy0 interface add freenet1 type __ap");
	system("sleep 2");
	system("sudo ifconfig freenet0 up");
	system("sudo ifconfig freenet1 hw ether 64:27:37:E9:95:e5");
	system("sudo ifconfig freenet1 192.168.0.1 up");

//	system("sudo nmcli nm wifi off");
//	system("sudo stop network-manager");
//	system("sudo rfkill unblock wlan");
//	system("sudo ifconfig wlan0 down");
//	sprintf(command,"sudo ifconfig %s hw ether 60:6c:66:88:31:3c",ifcName);
//	system(command);
//	sprintf(command,"sudo ifconfig %s 10.10.0.1/24 up",ifcName);
//	system(command);
//	sprintf(command,"sudo iw dev %s interface add vwlan0 type managed",ifcName);
//	system(command);
//	sprintf(command,"sudo ip link set dev vwlan0 address %s",mac);
//	system(command);
//	system("sudo ip link set dev vwlan0 up");
	g_print("Created Environment for freenet\n");

}

int main(int argc,char **argv){
	GError *error = NULL;
	GtkWidget *window;
	GtkWidget *label;

    gtk_init(&argc,&argv);
	
	appStarted=0;
	hotspotStatus=0;
	ftpStatus=0;
	connected=0;

	GtkWidget *darea = gtk_drawing_area_new();
	
	mainBuilder = gtk_builder_new();
	if(!gtk_builder_add_from_file(mainBuilder,"Interfaces/mainWindow.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return 1;
    }
   	window = GTK_WIDGET(gtk_builder_get_object(mainBuilder,"main_window"));

	gtk_container_add(GTK_CONTAINER(gtk_builder_get_object(mainBuilder,"profile_icon_button")), darea);

  	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL);  
	gtk_widget_set_size_request(darea,52,52);	
	gtk_style_context_add_class(gtk_widget_get_style_context(darea),"profileArea");
	gtk_widget_show_all(darea);	

	css_provider= gtk_css_provider_new ();
	gtk_css_provider_load_from_path(css_provider,"styles/main.css",NULL);


	GdkDisplay *display = gdk_display_get_default ();
	GdkScreen *screen = gdk_display_get_default_screen (display);

	gtk_style_context_add_provider_for_screen (screen,
                                           GTK_STYLE_PROVIDER (css_provider),
                                           GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    gtk_builder_connect_signals(mainBuilder,NULL);	
    gtk_widget_show(window);
	gtk_widget_set_size_request(window,250,375);

	wifiBuilder = gtk_builder_new();
	if(!gtk_builder_add_from_file(wifiBuilder,"Interfaces/wifiBox.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return 1;
    }
    gtk_builder_connect_signals(wifiBuilder,NULL);	

	hotspotBuilder = gtk_builder_new();
	if(!gtk_builder_add_from_file(hotspotBuilder,"Interfaces/hotspotBox.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return 1;
    }
    gtk_builder_connect_signals(hotspotBuilder,NULL);	

	pthread_create(&thread,NULL,createEnvironment,NULL);
	
	label=GTK_WIDGET(gtk_builder_get_object(mainBuilder,"wifi_main_label"));
	gtk_misc_set_alignment(GTK_MISC (label), 0.0, 0.95);
	label=GTK_WIDGET(gtk_builder_get_object(mainBuilder,"wifi_small_label"));
	gtk_misc_set_alignment(GTK_MISC (label), 0.0, 0.05);
	
	setStartScreen();
	appStarted=1;

	gtk_main();

    return 0;
}
gboolean imageTooltip(GtkStatusIcon *status_icon, gint x, gint y, gboolean keyboard_mode, GtkTooltip *tooltip, gpointer user_data){
	
	GdkPixbuf *pixbuf;
	pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile.png", -1, 300, NULL);
	GtkWidget *bigImage=gtk_image_new();
	gtk_image_set_from_pixbuf(GTK_IMAGE(bigImage),pixbuf);
	gtk_widget_show(bigImage);
	gtk_tooltip_set_custom(tooltip,bigImage);
}
