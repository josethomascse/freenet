#include"main.h"
#include<gtk/gtk.h>
#include"startScreen.h"
#include <cairo.h>

void getConnectedEssidName(){
	GtkWidget *label;
	GtkWidget *label2;
	GtkWidget *signalImage;
	FILE *fp;
	fp=popen("iwconfig freenet0 | grep ESSID | cut -d: -f2","r");
	char str[100],qstr[10];
	int len,signal;
	fgets(str,100,fp);
	len=strlen(str);
	str[len-3]='\0';
	label=GTK_WIDGET(gtk_builder_get_object(mainBuilder,"wifi_main_label"));
	label2=GTK_WIDGET(gtk_builder_get_object(mainBuilder,"wifi_small_label"));
	signalImage=GTK_WIDGET(gtk_builder_get_object(mainBuilder,"main_signal_image"));	
	if(strcmp(str,"off/any")==0){
		g_print("No Connections found\n");
		gtk_label_set_text(GTK_LABEL(label),"Not Connected");
		gtk_label_set_text(GTK_LABEL(label2),"click to connect");
		gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/wifi-icon.png");
	}
	else{
		g_print("Connected to Some Essid\n");
		pclose(fp);
		fp=popen("iwconfig freenet0 | grep Quality | awk '{ print $2 }' | cut -c9-","r");
		fgets(qstr,10,fp);
		qstr[2]='\0';
		signal=atoi(qstr);
		printf("signal %d\n",signal);
		signal=(signal-15)/11;
		switch(signal){
			case 0:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/wifi-icon6.png");
				break;
			case 1:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/wifi-icon5.png");
				break;
			case 2:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/wifi-icon4.png");
				break;
			case 3:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/wifi-icon3.png");
				break;
			case 4:
			case 5:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/wifi-icon2.png");
				break;		
		}
		str[len-4]='\0';		
		len=strlen(str);
		if(len>14){
			str[13]='.';
			str[14]='.';
			str[15]='\0';
		}
		gtk_label_set_text(GTK_LABEL(label),str+1);
		gtk_label_set_text(GTK_LABEL(label2),"connected");
	}
	pclose(fp);
}
void setStartScreen(){
   
 	GtkWidget *bottomBox;
	
	GtkWidget *grid;

    GError *error = NULL;
	GList *children, *iter;

	g_print("Refreshing start screen\n");
	
    bottomBox = GTK_WIDGET(gtk_builder_get_object(mainBuilder,"bottom_box"));	

	children = gtk_container_get_children(GTK_CONTAINER(bottomBox));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(bottomBox),GTK_WIDGET(iter->data));
	grid=GTK_WIDGET(gtk_builder_get_object(mainBuilder,"start_bottom_grid"));
	gtk_container_add(GTK_CONTAINER(bottomBox),grid);
	
	if(appStarted){
		getConnectedEssidName();
	}
	g_list_free(children);
	

}
