#include"main.h"
#include"ftp.h"
void ftpAction(GtkWidget *widget,gpointer data){
	if(ftpStatus){
		system("sudo service proftpd stop");
		ftpStatus=0;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"fileshare_image"));
		gtk_image_set_from_file(img,"Interfaces/server1.png");	
		system("notify-send -i /home/jose/Desktop/freenet/Interfaces/server1.png \"FTP Server\" \"Service stopped\"");
	}
	else{
		system("sudo service proftpd start");
		ftpStatus=1;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"fileshare_image"));
		gtk_image_set_from_file(img,"Interfaces/server.png");
		system("notify-send -i /home/jose/Desktop/freenet/Interfaces/server.png \"FTP Server\" \"Service started\"");
	}
}
void goToFtpScreen(GtkWidget *widget,gpointer data){
	g_print("Ftp screen\n");
	GtkWidget *grid;
	GtkBuilder *tempBuilder;
    GError *error = NULL;
	GList *children, *iter;

	tempBuilder = gtk_builder_new();
	if(!gtk_builder_add_from_file(tempBuilder,"Interfaces/ftpBox.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return;
    }

	GtkWidget *bottomBox = GTK_WIDGET(gtk_builder_get_object(mainBuilder,"bottom_box"));
	children = gtk_container_get_children(GTK_CONTAINER(bottomBox));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(bottomBox),GTK_WIDGET(iter->data));
	grid=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"grid1"));
	gtk_container_add(GTK_CONTAINER(bottomBox),grid);

	gtk_builder_connect_signals(tempBuilder,NULL);

	GtkWidget *label = GTK_WIDGET(gtk_builder_get_object(tempBuilder,"label1"));
	GtkWidget *button = GTK_WIDGET(gtk_builder_get_object(tempBuilder,"button2"));
	if(connected){
		FILE *fp;
		char command[100],str[100];
		fp=popen("sudo ifconfig freenet0 | grep \'inet addr\' | awk \'{ print $2 \'} | cut -c6-","r");
		fgets(str,100,fp);
		str[strlen(str)-1]='\0';
		sprintf(command,"ftp://%s",str);
		gtk_label_set_text(GTK_LABEL(label),command);
		pclose(fp);
	}
	else{
		gtk_widget_hide(button);
	}
	label = GTK_WIDGET(gtk_builder_get_object(tempBuilder,"label2"));
	button = GTK_WIDGET(gtk_builder_get_object(tempBuilder,"button1"));
	if(hotspotStatus){
		FILE *fp;
		char command[100],str[100];
		fp=popen("sudo ifconfig freenet1 | grep \'inet addr\' | awk \'{ print $2 \'} | cut -c6-","r");
		fgets(str,100,fp);
		str[strlen(str)-1]='\0';
		sprintf(command,"ftp://%s",str);
		gtk_label_set_text(GTK_LABEL(label),command);
		pclose(fp);
	}
	else{
		gtk_widget_hide(button);
	}


	g_list_free(children);
}

