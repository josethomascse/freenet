#include"main.h"
#include"wifi.h"
#include<pthread.h>
#include<string.h>
int wifiStatus=1;
char wifiList[100][100];
int wifiSig[100];
int k;
int focused;
int wifiProt[100];
GtkWidget *wNode[100];	
GtkWidget *wImg[100];
GtkWidget *wifiButton[100];
GtkWidget *connectButton;
GtkWidget *passBox;
int wifiClicked[100];
pthread_t wifiThread;
int conId;
int macMenu=0;
int scan=1;
char connectedEssid[100]="(none)";
void goToConnectionScreen();
GtkWidget *macIdBox;

void connectionNotification(){
	FILE *fp;
	char qstr[10],command[100],str[100];	
	fp=popen("iwconfig freenet0 | grep Quality | awk '{ print $2 }' | cut -c9-","r");
	fgets(qstr,10,fp);
	qstr[2]='\0';
	int signal=atoi(qstr);
	signal=(signal-15)/11;
	switch(signal){
		case 0:	strcpy(str,"/home/jose/Desktop/freenet/Interfaces/wifi-icon6.png");
			break;
		case 1:	strcpy(str,"/home/jose/Desktop/freenet/Interfaces/wifi-icon5.png");
			break;
		case 2:	strcpy(str,"/home/jose/Desktop/freenet/Interfaces/wifi-icon4.png");
			break;
		case 3:	strcpy(str,"/home/jose/Desktop/freenet/Interfaces/wifi-icon3.png");
			break;
		case 4:
		case 5:	strcpy(str,"/home/jose/Desktop/freenet/Interfaces/wifi-icon2.png");
			break;		
	}
	sprintf(command,"notify-send -i %s \'%s\' \'Connected\'",str,connectedEssid);
	printf("%s",command);
	system(command);
	pclose(fp);
}

void openWifiConfig(char *str){
	FILE *fp;
	char temp[100];
	fp=fopen("wpa.conf","w");
	sprintf(temp,"network={\n\tssid=\"%s\"\n\tkey_mgmt=NONE\n}",str);
	fputs(temp,fp);
	fclose(fp);
}
void wifiConnect(GtkWidget *node,gpointer data){
//	printf("Connection %s\nProtection %d\n",wifiList[focused],wifiProt[focused]);
	char command[100];
	int flag=0;
	if(strcmp(wifiList[focused],connectedEssid)==0){
		system("sudo dhclient -r freenet0");		
		char pid[20];			
		FILE *fp;
		fp=popen("pidof wpa_supplicant","r");
		fgets(pid,20,fp);
		sprintf(command,"sudo kill -SIGTERM %s",pid);
		system(command);
		system("notify-send -i /home/jose/Desktop/freenet/Interfaces/wifi-icon.png \'Disconnected\' \'You are now offline\'");
		printf("%s Disconnected\n",connectedEssid);
		strcpy(connectedEssid,"(none)");
		connected=0;
		sleep(1);
		system("sudo ifconfig freenet0 up");
	}
	else{
		if(connected){
			int temp=focused;
			focused=conId;
			scan=0;
			wifiConnect(NULL,NULL);
			scan=1;		
			focused=temp;
		}
		if(wifiProt[focused]){
			sprintf(command,"sudo wpa_passphrase \'%s\' \'%s\' > wpa.conf",wifiList[focused],gtk_entry_get_text(GTK_ENTRY(passBox)));	
			system(command);
		}	
		else{
			openWifiConfig(wifiList[focused]);
		}
		char str[100];
		FILE *fp;
		system("sudo wpa_supplicant -B -ifreenet0 -cwpa.conf");
		sleep(3);
		fp=popen("sudo ifconfig freenet0 | grep RUNNING","r");
		fgets(str,50,fp);
		if(strlen(str)>5){
			printf("Entered connecting page");
			system("sudo dhclient freenet0");
			strcpy(connectedEssid,wifiList[focused]);	
			connected=1;
			connectionNotification();
			printf("%s Connected\n",connectedEssid);
			pclose(fp);					
		}
		else{
			pclose(fp);
			char pid[20];			
			fp=popen("pidof wpa_supplicant","r");
			fgets(pid,20,fp);
			sprintf(str,"sudo kill -SIGTERM %s",pid);
			system(str);
			printf("killed process %s\n",pid);
			pclose(fp);		
			sleep(1);
			system("notify-send -i /home/jose/Desktop/freenet/Interfaces/wifi-icon.png \"Can\'t Connect to the network\" \"Disconnected\"");
		}
	}
	if(scan)
		goToConnectionScreen();
}
void wifiConnectionMenu(GtkWidget *node,gpointer data){
	int i,j,flag=1,temp=0;
	for(i=0;i<k;i++){
		if(wifiButton[i]==node){
			break;
		}
	}
	GList *children = gtk_container_get_children(GTK_CONTAINER(node));
	GtkWidget *box=GTK_WIDGET(children->data);
	if(wifiClicked[i]==0){
		focused=i;
  		GtkWidget *button;
		if(strcmp(connectedEssid,wifiList[i])==0)
			temp=1;
		if(wifiProt[i] && !temp){
			GtkWidget *pbox=GTK_WIDGET(gtk_entry_new());
			gtk_entry_set_placeholder_text(GTK_ENTRY(pbox),"Password");	
			gtk_entry_set_visibility(GTK_ENTRY(pbox),FALSE);
			//gtk_entry_set_invisible_char(GTK_ENTRY(pbox),'*');	
			gtk_widget_show(pbox);
			gtk_box_pack_start(GTK_BOX(box),pbox,TRUE,TRUE,10);
			passBox=pbox;
		}
		if(temp)
			button=GTK_WIDGET(gtk_button_new_with_label("Disconnect"));
		else
			button=GTK_WIDGET(gtk_button_new_with_label("Connect"));
		gtk_widget_show(button);
		gtk_box_pack_start(GTK_BOX(box),button,TRUE,TRUE,0);
		g_signal_connect (button, "clicked", G_CALLBACK (wifiConnect),NULL);
		connectButton=button;
		flag=0;	
		for(j=0;j<k;j++){
			if(wifiClicked[j]==1){
				flag=1;
				break;
			}
		}
		wifiClicked[i]=1;
		i=j;
		box=wNode[i];
	}
	if(flag){
		wifiClicked[i]=0;
		children = gtk_container_get_children(GTK_CONTAINER(box));
		while(children!=NULL){
			if(!GTK_IS_BOX(children->data)){	
				gtk_container_remove(GTK_CONTAINER(box),GTK_WIDGET(children->data));					
			} 
		 	children=g_list_next(children);
		}
	}
}

GtkWidget *newWifiBlock(char *essid,char *fullid,int signal,int protection,int k){
    GtkWidget *box;
	GtkWidget *hbox;
    GtkWidget *label;
	GtkWidget *signalImage;
  
	signal=(signal-15)/11;	

	box=gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	hbox=gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);

    label = gtk_label_new(essid);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	switch(signal){
		case 0:	signalImage=gtk_image_new_from_file("Interfaces/s5.png");
				break;
		case 1:	signalImage=gtk_image_new_from_file("Interfaces/s6.png");
				break;
		case 2:	signalImage=gtk_image_new_from_file("Interfaces/s7.png");
				break;
		case 3:	signalImage=gtk_image_new_from_file("Interfaces/s8.png");
				break;
		case 4:
		case 5:	signalImage=gtk_image_new_from_file("Interfaces/s9.png");
				break;		
	}
	wImg[k]=signalImage;

    gtk_box_pack_start(GTK_BOX(hbox),signalImage,FALSE,FALSE,5);
    gtk_box_pack_start(GTK_BOX(hbox),label,TRUE, TRUE,5);
	if(strcmp(connectedEssid,fullid)==0){
		GtkWidget *connectedImage;
		conId=k;
	    connectedImage=gtk_image_new_from_file("Interfaces/connected.png");  
		if(protection)  
			gtk_box_pack_start(GTK_BOX(hbox),connectedImage,FALSE,FALSE,0);
		else
			gtk_box_pack_start(GTK_BOX(hbox),connectedImage,FALSE,FALSE,10);
		gtk_widget_show(connectedImage);
	}
	if(protection){
		GtkWidget *securityImage;
	    securityImage=gtk_image_new_from_file("Interfaces/lock.png");    
		gtk_box_pack_start(GTK_BOX(hbox),securityImage,FALSE,FALSE,10);
		gtk_widget_show(securityImage);
	}

	gtk_box_pack_start(GTK_BOX(box),hbox,FALSE,FALSE,0);
    gtk_widget_show(label);
	gtk_widget_show(signalImage);
	gtk_widget_show(hbox);

    return box;
}
void changeSignalImage(GtkWidget *signalImage,int signal){
	signal=(signal-15)/11;
	switch(signal){
		case 0:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/s5.png");
				break;
		case 1:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/s6.png");
				break;
		case 2:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/s7.png");
				break;
		case 3:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/s8.png");
				break;
		case 4:
		case 5:	gtk_image_set_from_file(GTK_IMAGE(signalImage),"Interfaces/s9.png");
				break;		
	}
}
void goToConnectionScreen(){
	GtkWidget *bottomBox;
	GtkWidget *grid;
 	GtkWidget *button;
	GtkWidget *node;
	GtkWidget *wifiScroller;
	GList *children, *iter;
	GtkBuilder *tempBuilder;
	GError *error=NULL;
	GtkWidget *swindow;
	GtkWidget *vport;

	tempBuilder = gtk_builder_new();
    if(!gtk_builder_add_from_file(tempBuilder,"Interfaces/wifiBox.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return;
    }
    bottomBox = GTK_WIDGET(gtk_builder_get_object(mainBuilder,"bottom_box"));
	children = gtk_container_get_children(GTK_CONTAINER(bottomBox));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(bottomBox),GTK_WIDGET(iter->data));
	grid=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"grid1"));
	swindow=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"scrolledwindow1"));
	vport=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"paned1"));
	gtk_container_add(GTK_CONTAINER(swindow),vport);
	gtk_container_add(GTK_CONTAINER(bottomBox),swindow);
	
	FILE *fp;
	fp=popen("sudo iwlist freenet0 s | grep \"ESSID:\\|Quality\\|Encryption key\"","r");
	int i,j,sig,prot,maxLen,flag;	
	char str[100],t[100],t2[100];
	k=0;
	while(fgets(str,100,fp)){
		flag=1;
		for(i=0;str[i]!='=';++i);
		t[0]=str[++i];
		t[1]=str[++i];
		t[2]='\0';
		sig=atoi(t);
		
		fgets(str,100,fp);
		for(i=0;str[i]!=':';++i);
		if(str[i+2]=='f')
			prot=0;
		else
			prot=1;

		fgets(str,100,fp);
		for(i=0;str[i]!='\"';++i);
		for(i++,j=0;str[i]!='\"';++i,++j)
			t[j]=str[i];
		t[j]='\0';
		maxLen=strlen(t);
		strcpy(t2,t);
		if(maxLen>13){
			t[14]='.';
			t[15]='.';
			t[16]='.';
			t[17]='\0';
		}

		for(i=0;i<k;i++){
			if(strcmp(wifiList[i],t2)==0){
				flag=0;
				if(wifiSig[i]<sig)
					changeSignalImage(wImg[i],sig);
				break;
			}
		}
		if(strcmp(t,"")==0)
			flag=0;
		if(flag){				
			strcpy(wifiList[k],t2);
			button=GTK_WIDGET(gtk_button_new());
			node=newWifiBlock(t,t2,sig,prot,k);
			wNode[k]=node;
			wifiClicked[k]=0;
			wifiSig[k]=sig;
			wifiProt[k]=prot;
			gtk_container_add(GTK_CONTAINER(button),node);
			gtk_widget_show(node);
			gtk_widget_show(button);
			gtk_style_context_add_class(gtk_widget_get_style_context(node),"wifiNode");
			gtk_grid_attach(GTK_GRID(grid),button,0,k,3,1);
			gtk_container_set_border_width(GTK_CONTAINER (button), 0);
			g_signal_connect (button, "clicked", G_CALLBACK (wifiConnectionMenu), NULL);
			wifiButton[k]=button;
			k++;	
		}
	}
	pclose(fp);

	g_list_free(children);
}

void scanWifi(GtkWidget *widget,gpointer data){
	g_print("Scanning Wifi..\n");
	goToConnectionScreen();
}

void wifiAction(GtkWidget *widget,gpointer data){
	if(wifiStatus){
		system("sudo ifconfig freenet0 down");
		wifiStatus=0;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"wifi_button_image"));
		gtk_image_set_from_file(img,"Interfaces/wifi1.png");	
	}
	else{
		system("sudo ifconfig freenet0 up");
		wifiStatus=1;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"wifi_button_image"));
		gtk_image_set_from_file(img,"Interfaces/wifi.png");
	}
}
void changeMacIdScreen(GtkWidget *widget,gpointer data);
void goToSettingsScreen(GtkWidget *widget,gpointer data){
	GtkWidget *label;
	GtkWidget *bottomBox;
	GList *children,*iter;
	GtkWidget *grid;

    bottomBox = GTK_WIDGET(gtk_builder_get_object(mainBuilder,"bottom_box"));
	children = gtk_container_get_children(GTK_CONTAINER(bottomBox));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(bottomBox),GTK_WIDGET(iter->data));
	grid=GTK_WIDGET(gtk_builder_get_object(wifiBuilder,"grid2"));
	gtk_container_add(GTK_CONTAINER(bottomBox),grid);

	FILE *fp,*fptr;
	fp=popen("sudo ifconfig freenet0 | grep HWaddr | awk \'{ print $5 \'}","r");
	char str[30];
	fgets(str,30,fp);	
	label=GTK_WIDGET(gtk_builder_get_object(wifiBuilder,"macLabel"));
	str[17]='\0';
	gtk_label_set_text(GTK_LABEL(label),str);
	pclose(fp);
	
	fp=popen("sudo ifconfig freenet0 | grep bytes | awk \'{ print $2 \'} | cut -c7-","r");
	fgets(str,30,fp);	
	long long int ustr;
	double usage;
	ustr=atoll(str);
	ustr/=(1024*1024);
	usage=ustr;
	pclose(fp);	
	label=GTK_WIDGET(gtk_builder_get_object(wifiBuilder,"sessionLabel"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);	
	if(usage>=1024){
		usage/=1024;
		sprintf(str,"%0.2lf GB",usage);
	}
	else
		sprintf(str,"%0.2lf MB",usage);
	gtk_label_set_text(GTK_LABEL(label),str);
		
	fptr=fopen("wifiUsage","r");
	fgets(str,30,fptr);
	ustr+=atoll(str);
	fclose(fptr);
	usage=(double)ustr;
	label=GTK_WIDGET(gtk_builder_get_object(wifiBuilder,"totalLabel"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);
	if(usage>=1024){
		usage/=1024;
		sprintf(str,"%0.2lf GB",usage);
	}
	else{
		sprintf(str,"%0.2lf MB",usage);
	}	
	gtk_label_set_text(GTK_LABEL(label),str);

	g_list_free(children);

}
void changeMacId(GtkWidget *widget,gpointer data){
	printf("Entered");	
	if(connected){
		focused=conId;
		scan=0;
		wifiConnect(NULL,NULL);
		scan=1;		
	}
	char command[100];
	system("sudo ifconfig freenet0 down");
	sprintf(command,"sudo ifconfig freenet0 hw ether %s",gtk_entry_get_text(GTK_ENTRY(macIdBox)));
	printf("%s",command);
	system(command);
	system("sudo ifconfig freenet0 up");
	GList *children;
	GtkWidget *macButtonBox=GTK_WIDGET(gtk_builder_get_object(wifiBuilder,"box1"));
	children = gtk_container_get_children(GTK_CONTAINER(macButtonBox));
	while(children!=NULL){
		if(!GTK_IS_LABEL(children->data)){	
			gtk_container_remove(GTK_CONTAINER(macButtonBox),GTK_WIDGET(children->data));					
		} 
	 	children=g_list_next(children);
	}
	macMenu=0;
	goToSettingsScreen(NULL,NULL);
}
void changeMacIdScreen(GtkWidget *widget,gpointer data){
	GtkWidget *macButtonBox;
	if(macMenu==0){
		macMenu=1;
		GtkWidget *button;
		macButtonBox=GTK_WIDGET(gtk_builder_get_object(wifiBuilder,"box1"));
		macIdBox=GTK_WIDGET(gtk_entry_new());
		gtk_entry_set_placeholder_text(GTK_ENTRY(macIdBox),"Set new MacId:");	
		gtk_box_pack_start(GTK_BOX(macButtonBox),macIdBox,TRUE,TRUE,0);
		gtk_widget_show(macIdBox);
		button=gtk_button_new_with_label("Save");
		g_signal_connect (button, "clicked", G_CALLBACK (changeMacId),NULL);	
		gtk_box_pack_start(GTK_BOX(macButtonBox),button,TRUE,TRUE,0);
		gtk_widget_show(button);
	}
	else{
		GList *children;
		macButtonBox=GTK_WIDGET(gtk_builder_get_object(wifiBuilder,"box1"));
		children = gtk_container_get_children(GTK_CONTAINER(macButtonBox));
		while(children!=NULL){
			if(!GTK_IS_LABEL(children->data)){	
				gtk_container_remove(GTK_CONTAINER(macButtonBox),GTK_WIDGET(children->data));					
			} 
		 	children=g_list_next(children);
		}
		macMenu=0;
	}
}

