#include"main.h"
#include <gtk/gtk.h>
#include"hotspot.h"
#include<stdlib.h>
struct hotspotConfig{
	char essId[100];
	char pass[100];
	int type;
};
void initializeHotspotNode();

void createHotspotConf(struct hotspotConfig *hNode){
	FILE *fp;
	fp=fopen("hotspot.conf","w");
	char str[400];
	if(hNode->type){
		sprintf(str,"interface=freenet1\ndriver=nl80211\nssid=%s\nhw_mode=g\nchannel=1\nmacaddr_acl=0\nauth_algs=1\nignore_broadcast_ssid=0\nwpa=3\nwpa_passphrase=%s\nwpa_key_mgmt=WPA-PSK\nwpa_pairwise=TKIP\nrsn_pairwise=CCMP",hNode->essId,hNode->pass);
	}
	else{
		sprintf(str,"interface=freenet1\ndriver=nl80211\nssid=%s\nhw_mode=g\nchannel=1\nmacaddr_acl=0\nauth_algs=1\nignore_broadcast_ssid=0\nrsn_pairwise=CCMP",hNode->essId);
	}
	fputs(str,fp);
	fclose(fp);
}
void startHotspot(){
	struct hotspotConfig *hNode;
	FILE *fp=fopen("hotspotNode","r");
	hNode=(struct hotspotConfig *)malloc(sizeof(struct hotspotConfig));
	if(fp==NULL){
		printf("hotspot.conf not found\n");
		initializeHotspotNode();
		fp=fopen("hotspotNode","r");
	}
	fread(hNode,sizeof(struct hotspotConfig),1,fp);
	createHotspotConf(hNode);
	system("sudo ifconfig freenet1 up");
	system("sudo hostapd -B hotspot.conf");
	system("sleep 2");
	system("sudo udhcpd dhcp.conf");
/*//	system("sudo service hostapd start");
	system("echo 1|sudo tee /proc/sys/net/ipv4/ip_forward");
//	system("sudo iptables -t nat -A POSTROUTING -s 10.10.0.0/16 -o vwlan0 -j MASQUERADE");
//	system("echo \"1\" > /proc/sys/net/ipv4/ip_forward");
	system("sudo iptables --table nat --append POSTROUTING --out-interface freenet0 -j MASQUERADE");
	system("sudo iptables --append FORWARD --in-interface freenet1 -j ACCEPT");*/
}
void stopHotspot(){
	char pid[20],command[100];			
	FILE *fp;
	fp=popen("pidof udhcpd","r");
	fgets(pid,20,fp);
	sprintf(command,"sudo kill -SIGTERM %s",pid);
	system(command);
	pclose(fp);
	fp=popen("pidof hostapd","r");
	fgets(pid,20,fp);
	sprintf(command,"sudo kill -SIGTERM %s",pid);
	system(command);
	pclose(fp);

	fp=fopen("hotspotNode","r");
	struct hotspotConfig *hNode=(struct hotspotConfig *)malloc(sizeof(struct hotspotConfig));
	fread(hNode,sizeof(struct hotspotConfig),1,fp);
	sprintf(command,"notify-send -i /home/jose/Desktop/freenet/Interfaces/hotspot1.png \"%s\" \"Hotspot stopped\"",hNode->essId);
	system(command);
	fclose(fp);
}
void hotspotAction(GtkWidget *widget,gpointer data){
	if(hotspotStatus){
		stopHotspot();
		hotspotStatus=0;
		GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"hotspot_button_image"));
		gtk_image_set_from_file(img,"Interfaces/hotspot1.png");	
	}
	else{
		startHotspot();
		FILE *fp;
		char str[50];
		sleep(2);
		fp=popen("sudo ifconfig freenet1 | grep RUNNING","r");
		fgets(str,50,fp);
		fclose(fp);
		fp=fopen("hotspotNode","r");
		struct hotspotConfig *hNode=(struct hotspotConfig *)malloc(sizeof(struct hotspotConfig));
		fread(hNode,sizeof(struct hotspotConfig),1,fp);
		char command[200];

		if(strlen(str)>5){
			hotspotStatus=1;
			GtkImage *img=GTK_IMAGE(gtk_builder_get_object(mainBuilder,"hotspot_button_image"));
			gtk_image_set_from_file(img,"Interfaces/hotspot.png");
			sprintf(command,"notify-send -i /home/jose/Desktop/freenet/Interfaces/hotspot.png \"%s\" \"Hotspot started\"",hNode->essId);
			system(command);
		}
		else{
			sprintf(command,"notify-send -i /home/jose/Desktop/freenet/Interfaces/hotspot1.png \"%s\" \"Can't start hotspot\"",hNode->essId);
			system(command);	
		}
		fclose(fp);
	}
}
void initializeHotspotNode(){
	FILE *fp=fopen("hotspotNode","w");
	struct hotspotConfig *hNode;
	hNode=(struct hotspotConfig *)malloc(sizeof(struct hotspotConfig));
	if(fp==NULL){
		printf("Can't Update\n");
		return;
	}
	strcpy(hNode->essId,"freenet");
	hNode->type=0;
	fwrite(hNode,sizeof(struct hotspotConfig),1,fp);
	fclose(fp);
}
void goToHotspotScreen(GtkWidget *widget,gpointer data){
	g_print("hotspot screen\n");
	GtkBuilder *tempBuilder;
	GtkWidget *grid;
    GError *error = NULL;
	GList *children, *iter;

    GtkWidget *bottomBox = GTK_WIDGET(gtk_builder_get_object(mainBuilder,"bottom_box"));
	children = gtk_container_get_children(GTK_CONTAINER(bottomBox));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(bottomBox),GTK_WIDGET(iter->data));
	grid=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"grid1"));
	gtk_container_add(GTK_CONTAINER(bottomBox),grid);

	FILE *fp;
	fp=popen("sudo ifconfig freenet1 | grep HWaddr | awk \'{ print $5 \'}","r");
	char str[30];
	fgets(str,30,fp);	
	GtkWidget *label=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"hmacLabel"));
	str[17]='\0';
	gtk_label_set_text(GTK_LABEL(label),str);
	pclose(fp);

	printf("Reached new session\n");
	struct hotspotConfig *hNode;
	fp=fopen("hotspotNode","r");
	hNode=(struct hotspotConfig *)malloc(sizeof(struct hotspotConfig));
	if(fp==NULL){
		printf("hotspot.conf not found\n");
		initializeHotspotNode();
		goToHotspotScreen(NULL,NULL);	
	}
	else{
		printf("hotspot.conf found\n");
		fread(hNode,sizeof(struct hotspotConfig),1,fp);
		GtkWidget *label=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"essidLabel"));
		gtk_label_set_text(GTK_LABEL(label),hNode->essId);
		label=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"pskLabel"));
		if(hNode->type){
			gtk_label_set_text(GTK_LABEL(label),hNode->pass);
		}
		else{
			gtk_label_set_text(GTK_LABEL(label),"open");
		}
		fclose(fp);
	}
	g_list_free(children);
}
int machMenu=0;
GtkWidget *machIdBox;
void changehMacId(GtkWidget *widget,gpointer data){
	printf("Entered");	
	if(hotspotStatus){
		stopHotspot();
	}
	char command[100];
	system("sudo ifconfig freenet1 down");
	sprintf(command,"sudo ifconfig freenet1 hw ether %s",gtk_entry_get_text(GTK_ENTRY(machIdBox)));
	printf("%s",command);
	system(command);
	system("sudo ifconfig freenet1 192.168.0.1 up");

	GList *children;
	GtkWidget *macButtonBox=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"box1"));
	children = gtk_container_get_children(GTK_CONTAINER(macButtonBox));
	while(children!=NULL){
		if(!GTK_IS_LABEL(children->data)){	
			gtk_container_remove(GTK_CONTAINER(macButtonBox),GTK_WIDGET(children->data));					
		} 
	 	children=g_list_next(children);
	}
	machMenu=0;
	goToHotspotScreen(NULL,NULL);
}
void changehMacIdScreen(GtkWidget *widget,gpointer data){
	GtkWidget *macButtonBox;
	if(machMenu==0){
		machMenu=1;
		GtkWidget *button;
		macButtonBox=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"box1"));
		machIdBox=GTK_WIDGET(gtk_entry_new());
		gtk_entry_set_placeholder_text(GTK_ENTRY(machIdBox),"Set new MacId:");	
		gtk_box_pack_start(GTK_BOX(macButtonBox),machIdBox,TRUE,TRUE,0);
		gtk_widget_show(machIdBox);
		button=gtk_button_new_with_label("Save");
		g_signal_connect (button, "clicked", G_CALLBACK (changehMacId),NULL);	
		gtk_box_pack_start(GTK_BOX(macButtonBox),button,TRUE,TRUE,0);
		gtk_widget_show(button);
	}
	else{
		GList *children;
		macButtonBox=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"box1"));
		children = gtk_container_get_children(GTK_CONTAINER(macButtonBox));
		while(children!=NULL){
			if(!GTK_IS_LABEL(children->data)){	
				gtk_container_remove(GTK_CONTAINER(macButtonBox),GTK_WIDGET(children->data));					
			} 
		 	children=g_list_next(children);
		}
		machMenu=0;
	}
}
void activatePassBox(GtkWidget *widget,gpointer data){
	GtkWidget *hotpBox=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"passhBox"));
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)))
		gtk_widget_set_sensitive(hotpBox,TRUE);
	else
		gtk_widget_set_sensitive(hotpBox,FALSE);
}
void changeHotspotConfiguration(GtkWidget *widget,gpointer data){
	FILE *fp;
	fp=fopen("hotspotNode","w");
	if(fp==NULL){
		printf("Can't Update\n");
		return;
	}
	printf("File Opened");
	GtkWidget *entry=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"entry1"));
	struct hotspotConfig *hNode;
	hNode=(struct hotspotConfig *)malloc(sizeof(struct hotspotConfig));
	strcpy(hNode->essId,gtk_entry_get_text(GTK_ENTRY(entry)));
	printf("%s\n",hNode->essId);
	if(strlen(hNode->essId)<2)
		return;
	gtk_entry_set_text(GTK_ENTRY(entry),"");

	GtkWidget *hotpBox=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"passhBox"));
	widget=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"photbutton")); 
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))){
		entry=GTK_WIDGET(gtk_builder_get_object(hotspotBuilder,"passhBox"));
		strcpy(hNode->pass,gtk_entry_get_text(GTK_ENTRY(entry)));
		if(strlen(hNode->pass)<9)
			return;
		hNode->type=1;
		printf("%s",hNode->pass);
		gtk_entry_set_text(GTK_ENTRY(entry),"");
	}
	else
		hNode->type=0;

	fwrite(hNode,sizeof(struct hotspotConfig),1,fp);
	fclose(fp);
	goToHotspotScreen(NULL,NULL);
}
