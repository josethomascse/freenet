#include"main.h"
#include<gtk/gtk.h>
#include"profile.h"
#include<stdlib.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

GtkBuilder *tempBuilder;
struct{
	cairo_surface_t *image;
}glob;
void loadProfileScreenImage(cairo_t *cr);
gboolean profile_draw_event(GtkWidget *widget,cairo_t *cr,gpointer user_data){      
 	loadProfileScreenImage(cr);
  	return FALSE;
}
void loadProfileScreenImage(cairo_t *cr){
	GdkPixbuf *pixbuf;

	pixbuf=gdk_pixbuf_new_from_file_at_size ("Interfaces/profile.png", -1, 36, NULL);
	glob.image = gdk_cairo_surface_create_from_pixbuf(pixbuf,0,NULL);
  	cairo_set_source_surface(cr, glob.image, 1, 1);
  	cairo_arc(cr,18, 18, 17, 0, 2*M_PI);
  	cairo_clip(cr);
  	cairo_paint(cr); 
 	g_object_unref (pixbuf);
}
void initializeProfileNode(){
	FILE *fp=fopen("profileNode","w");
	struct profileConfig *hNode;
	hNode=(struct profileConfig *)malloc(sizeof(struct profileConfig));
	if(fp==NULL){
		printf("Can't Update\n");
		return;
	}
	strcpy(hNode->name,"freenet-user");
	strcpy(hNode->key,"(none)");
	fwrite(hNode,sizeof(struct profileConfig),1,fp);
	fclose(fp);
}
void goToProfileScreen(GtkWidget *widget,gpointer data){
	g_print("Profile screen\n");
	GtkWidget *grid;
    GError *error = NULL;
	GList *children, *iter;

	tempBuilder = gtk_builder_new();
	if(!gtk_builder_add_from_file(tempBuilder,"Interfaces/profileBox.glade",&error)){
        g_warning("%s",error->message);
        g_free(error);
        return;
    }

	GtkWidget *darea = gtk_drawing_area_new();

	gtk_container_add(GTK_CONTAINER(gtk_builder_get_object(tempBuilder,"profButton")), darea);

  	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(profile_draw_event), NULL);  
	gtk_widget_set_size_request(darea,36,36);	
	gtk_style_context_add_class(gtk_widget_get_style_context(darea),"profileArea");
	gtk_widget_show_all(darea);	

	FILE *fp=fopen("profileNode","r");
	struct profileConfig *hNode=(struct profileConfig *)malloc(sizeof(struct profileConfig));
	if(fp==NULL){
		initializeProfileNode();
		fp=fopen("profileNode","r");
	}
	fread(hNode,sizeof(struct profileConfig),1,fp);
    GtkWidget *label = GTK_WIDGET(gtk_builder_get_object(tempBuilder,"profTitle"));	
	gtk_label_set_text(GTK_LABEL(label),hNode->name);
	fclose(fp);

    GtkWidget *bottomBox = GTK_WIDGET(gtk_builder_get_object(mainBuilder,"bottom_box"));
	children = gtk_container_get_children(GTK_CONTAINER(bottomBox));
	for(iter = children; iter != NULL; iter = g_list_next(iter))
	  	gtk_container_remove(GTK_CONTAINER(bottomBox),GTK_WIDGET(iter->data));
	grid=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"grid1"));
	gtk_container_add(GTK_CONTAINER(bottomBox),grid);

	GtkWidget *button=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"filechooserbutton1"));
 	//gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (button),"/home");
	gtk_builder_connect_signals(tempBuilder,NULL);
	g_list_free(children);
}
void changeProfileConfigurations(GtkWidget *widget,gpointer data){
	printf("Reached config profile\n");
	GtkWidget *button=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"filechooserbutton1"));
	char *fileName;
	fileName=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(button));
	GtkWidget *entry=GTK_WIDGET(gtk_builder_get_object(tempBuilder,"entry1"));
	const char *name;
	name=gtk_entry_get_text(GTK_ENTRY(entry));
	printf("%s %s\n",fileName,name);
	if(fileName!=NULL){
		char str[200];
		printf("Entered");
		sprintf(str,"sudo cp \"%s\" \"%s\"",fileName,"Interfaces/profile.png");
		system(str);
	}
	if(strlen(name)>=1){
		FILE *fp=fopen("profileNode","w");
		struct profileConfig *hNode=(struct profileConfig *)malloc(sizeof(struct profileConfig));
		strcpy(hNode->name,name);
		fwrite(hNode,sizeof(struct profileConfig),1,fp);
		fclose(fp);
	}
	goToProfileScreen(NULL,NULL);
}
